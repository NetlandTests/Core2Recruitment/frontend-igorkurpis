export const environment = {
  production: true,
  host: 'recruits.siennsoft.com',
  api: 'http://recruits.siennsoft.com/api/',
  token: 'access_token'
};
