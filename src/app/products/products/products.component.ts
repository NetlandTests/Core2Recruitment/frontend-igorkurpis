import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ProductsService } from '../shared/products.service';
import { Product } from '../shared/products.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private productsSubscription: Subscription;

  dataSource: MatTableDataSource<Product>;
  displayedColumns = ['productID', 'name', 'price', 'description'];
  pageSize = 5;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsSubscription = this.productsService.fetch().subscribe(products => {
      this.dataSource =  new MatTableDataSource<Product>(products);
      this.dataSource.paginator = this.paginator;
    });
  }

  ngOnDestroy() {
    if (this.productsSubscription) {
      this.productsSubscription.unsubscribe();
    }
  }

}
