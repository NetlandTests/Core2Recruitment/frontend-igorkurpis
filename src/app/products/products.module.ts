import { NgModule } from '@angular/core';
import { JwtModule } from '@auth0/angular-jwt';
import { SharedModule } from '../shared/shared.module';
import { ProductsRoutingModule } from './products-routing.module';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './shared/products.service';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    SharedModule,
    ProductsRoutingModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [ProductsService],
  declarations: [ProductsComponent]
})
export class ProductsModule { }
