import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from '../shared/guards/logged-in.guard';
import { ProductsComponent } from './products/products.component';

const productsRoutes: Routes = [
  {
    path: 'products',
    component: ProductsComponent,
    canActivate: [LoggedInGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(productsRoutes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {}
