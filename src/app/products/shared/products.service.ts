import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';

import { RestService } from '../../shared/services/rest.service';
import { Product } from './products.model';

@Injectable()
export class ProductsService {
  constructor(private restService: RestService) {}

  fetch(): Observable<Product[]> {
    return this.restService.get<Product[]>('Products');
  }
}
