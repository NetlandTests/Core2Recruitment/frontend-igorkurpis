import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

  constructor(private tokenService: TokenService, private router: Router) { }

  isLogged(): boolean {
    if (!this.tokenService.isToken()) {
      if (this.router.url !== '/login') {
        this.router.navigate(['/login']);
      }
      return false;
    }
    return true;
  }

}
