import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class TokenService {

  constructor( private jwtHelperService: JwtHelperService) { }

  isToken(): boolean {
    return this.jwtHelperService.tokenGetter() && !this.jwtHelperService.isTokenExpired();
  }

  set(token: string) {
    localStorage.setItem(environment.token, token);
  }

  remove() {
    localStorage.removeItem(environment.token);
  }
}
