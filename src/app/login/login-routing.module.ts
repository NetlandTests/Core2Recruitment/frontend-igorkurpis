import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogoutGuard } from '../shared/guards/logout.guard';
import { LoginComponent } from './login/login.component';

const loginRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LogoutGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(loginRoutes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
