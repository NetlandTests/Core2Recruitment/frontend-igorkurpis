import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { LoginService } from '../shared/login.service';
import { LoginRequest, Token } from '../shared/login.model';
import { TokenService } from '../../shared/services/token.service';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private loginSubscription: Subscription;
  private userAuth: LoginRequest;

  loginForm: FormGroup;
  invalidCredentials = false;
  loginError = false;

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    private tokenService: TokenService) {}

  ngOnInit() {
    this.loginForm  = this.formBuilder.group({
      UserName: ['', [Validators.required]],
      Password: ['', [Validators.required]],
    });
  }
  ngOnDestroy() {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }

  login() {
    if (this.loginForm.valid) {
      this.invalidCredentials = false;
      this.loginError = false;
      this.userAuth = this.loginForm.value;
      this.loginSubscription = this.loginService
        .login(this.userAuth)
        .subscribe(token => {
          this.tokenService.set(token.access_token);
          this.router.navigate(['/']);
        }, error => {
          if (error.error === 'Invalid credentials') {
            this.invalidCredentials = true;
          } else {
            this.loginError = true;
          }
        });
    }
  }
}
