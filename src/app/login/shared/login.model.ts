export class LoginRequest {
  UserName: string;
  Password: string;
}

export class Token {
  access_token: string;
  expires_in: number;
}
