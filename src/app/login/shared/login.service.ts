import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';

import { RestService } from '../../shared/services/rest.service';
import { LoginRequest, Token } from './login.model';

@Injectable()
export class LoginService {
  constructor(private restService: RestService) {}

  login(userAuth: LoginRequest): Observable<Token> {
    return this.restService.post<LoginRequest, Token>('Jwt', userAuth, true);
  }
}
