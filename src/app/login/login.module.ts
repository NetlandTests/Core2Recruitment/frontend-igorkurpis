import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';

import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { LoginService } from './shared/login.service';

@NgModule({
  imports: [
    SharedModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    LoginRoutingModule
  ],
  providers: [LoginService],
  declarations: [LoginComponent]
})
export class LoginModule {}
